import { LitElement, html, css } from 'lit-element';

class CssComponent  extends LitElement {
    static get properties(){
        return{
            otroStyle : {type:String};
        }
    }
  static get styles() {
    return css`
      div {
        color: ${unsafeCSS(this.otroStyle)};
      }
    `;
  }


  constructor() {
    super();
    this.otroStyle = "red";
  }

  render() {
    return html`
      <div>Contenido estilado </div>
    `;
  }
}

customElements.define('css-component', CssComponent);