import { LitElement, html, css } from 'lit-element';
import { classMap } from 'lit-html/directives/class-map';
import { styleMap } from 'lit-html/directives/style-map';

class AttrComponent extends LitElement {
    static get styles() {
        return css`
            .myDiv {background-color:blue;}
            .otraClase {border: 1px solid red;}
            `;
    }

    static get properties() {
        return {
            areax: {attribute: true},
            areay: {attribute: true}
        }
    }

    attributeChangedCallback(name, oldval, newval) {
        console.log("Atributo modificado: ", name, newval);
        super.attributeChangedCallback(name, oldval, newval);
    }

    constructor() {
        super();
        this.areax = 10;
        this.areay = 20;
    }

    render() {
        return html`
            <p>Areax: ${this.areax}</p>
            <p>Areay: ${this.areay}</p>
            <button @click="${this.changeAttributes}">Cambiar desde dentro</button>
        `;
    }

     changeAttributes() {
         console.log("changeAttributes");
         let rndval = Math.floor(Math.random()*100).toString();
         this.setAttribute('areax', rndval);
         this.setAttribute('areay', rndval);
     }

     updated(changedProperties) {
        changedProperties.forEach((oldValue, propName) => {
            console.log(`${propName} modificada. Valor anterior ${oldValue}`);
        });
     }

}

customElements.define('attr-component', AttrComponent);