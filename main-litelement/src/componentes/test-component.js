import { LitElement, html } from 'lit-element';

class Saludos extends LitElement {
    static get properties() {
        return {
            message: {type:String},
            title: {type:String}
        }
    }
    

    constructor() {
        super();
        this.message = "Mensaje privado";
    }
    render() {
        return html`
            <div>${this.message}</div>
        `;
    }

    
}
customElements.define('test-saludos', Saludos)