import { LitElement, html, css } from 'lit-element';

export class SuperComponent  extends LitElement {

  static get styles() {
    return css`
      button {
        width: 200px;
      }
    `;
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      
    `;
  }
}

customElements.define('super-component', SuperComponent);