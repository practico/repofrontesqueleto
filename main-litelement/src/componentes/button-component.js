import { LitElement, html } from 'lit-element';

class ButtonComponent extends LitElement {
    static get properties() {
        return {
            message: {type:String}
            
        }
    }
    

    constructor() {
        super();
        this.message = "Mensaje privado";
    }
    render() {
        return html`
            <div>${this.message}</div>
            <div><button @click="${this.clickHandler}">Probar</button></div>
        `;
    }

    clickHandler(e) {
        console.log(e.target);
        this.message = "Mensaje público";
    }
    
}
customElements.define('button-component', ButtonComponent)