import { LitElement, html, css } from 'lit-element';
import {SuperComponent} from './super-component';
class ChildComponent  extends SuperComponent {

  static get styles() {
    return [
        super.styles,
        css`
        button{
            color: red;
        }
        `
    ];
      
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      
    `;
  }
}

customElements.define('child-component', ChildComponent);