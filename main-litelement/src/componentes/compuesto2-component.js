import { LitElement, html, css } from 'lit-element';
import './compuesto1-component.js';
class Compuesto2Component  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <compuesto1-component></compuesto1-component>
      <p>Pie doc>/p>
    `;
  }
}

customElements.define('compuesto2-component', Compuesto2Component);