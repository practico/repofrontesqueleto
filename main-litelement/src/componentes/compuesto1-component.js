import { LitElement, html, css } from 'lit-element';

class Compuesto1Component  extends LitElement {

 headerTemplate(title) {
     return html`<h1>${title}</h1>`;
     
 }
 bodyTemplate(contenido) {
    return html`<p>${contenido}</p>`;
    
}
  static get properties() {
    return {
        title: {type:String},
        contenido: {type:String}
    };
  }

  constructor() {
    super();
    this.title = "titulo";
    this.contenido = "afafadffaaf";
  }

  render() {
    return html`
      ${this.headerTemplate(this.title)}
      ${this.bodyTemplate(this.contenido)}
    `;
  }
}

customElements.define('compuesto1-component', Compuesto1Component);