import { LitElement, html, css } from 'lit-element';

class GetsetComponente  extends LitElement {

  static get properties() {
    return {
        prop : {type:Number}
    };
    
  }
  set prob(val){
        let oldVal = this.privateProp;
        this.privateProp = Math.floor(val);
        this.requestUpdate (`prop`,oldVal);
  }
  get prob(){
    return this.privateProp;
  }

  constructor() {
    super();
    this.privateProp = 0;
  }

  render() {
    return html`
      <p>Prop: ${this.prop}</p>
      <button @click="${() => {this.prop = Math.random() *10}}
">Cambiar Prop</button>
    `;
  }
}

customElements.define('getset-component', GetsetComponente);