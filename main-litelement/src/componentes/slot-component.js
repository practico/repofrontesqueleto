import { LitElement, html } from 'lit-element';

class SlotComponent extends LitElement {
    static get properties() {
        return {
            message: {type:String},
            title: {type:String}
        }
    }
    

    constructor() {
        super();
        this.message = "Mensaje privado";
    }
    render() {
        return html`
            <div>
                <p>Texto de prueba</p>
                <slot name="medio"></slot>
                <p>Texto de prueba1</p>
                <slot name="final"></slot>
            </div>
        `;
    }

    
}
customElements.define('slot-component', SlotComponent)